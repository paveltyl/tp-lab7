//
//  AppDelegate.h
//  Lab7_task2
//
//  Created by Pavel Tyletsky on 23.04.17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

