//
//  main.m
//  Lab7_task2
//
//  Created by Syrovatnikov Nikita on 23.04.17.
//  Copyright © 2017 Syrovatnikov Nikita. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
