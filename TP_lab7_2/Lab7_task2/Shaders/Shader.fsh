//
//  Shader.fsh
//  Lab7_task2
//
//  Created by Pavel Tyletsky on 23.04.17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
