//
//  ViewController.swift
//  TP_lab7_3
//
//  Created by Pavel Tyletsky on 4/28/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var backgroundSwitch: UISwitch!

    @IBOutlet weak var switchIndicator: UILabel!

    @IBAction func backgroundSwitchTapped(_ sender: Any) {
        if backgroundSwitch.isOn {
            switchIndicator.text = "cube.jpg"
            view.backgroundColor = UIColor(patternImage: UIImage(named: "cube")!)
        }
        else
        {
            switchIndicator.text = "triangle.jpg"
            view.backgroundColor = UIColor(patternImage: UIImage(named: "triangle")!)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //switchIndicator.textColor = UIColor.green
        switchIndicator.text = "cube"
        view.backgroundColor = UIColor(patternImage: UIImage(named: "cube")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

