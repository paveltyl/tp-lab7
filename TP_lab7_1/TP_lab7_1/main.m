//
//  main.m
//  TP_lab7_1
//
//  Created by Nikita on 4/24/17.
//  Copyright © 2017 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
