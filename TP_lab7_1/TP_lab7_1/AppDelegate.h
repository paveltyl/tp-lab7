//
//  AppDelegate.h
//  TP_lab7_1
//
//  Created by Pavel Tyletsky on 4/24/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

