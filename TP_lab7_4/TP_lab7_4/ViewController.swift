//
//  ViewController.swift
//  TP_lab7_4
//
//  Created by Pavel Tyletsky on 4/28/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBAction func longPress(_ sender: Any) {
        gestureIndicator.text = "Long press gesture"
        gestureIndicator.backgroundColor = UIColor.orange
    }
    @IBAction func pinch(_ sender: Any) {
        gestureIndicator.text = "Pinch gesture"
        gestureIndicator.backgroundColor = UIColor.red
    }
    @IBAction func swipe(_ sender: Any) {
        gestureIndicator.text = "Swipe gesture"
        gestureIndicator.backgroundColor = UIColor.lightGray
    }
    @IBAction func rotation(_ sender: Any) {
        gestureIndicator.text = "Rotation gesture"
        gestureIndicator.backgroundColor = UIColor.blue
    }
    @IBAction func tap(_ sender: Any) {
        gestureIndicator.text = "Tap gesture"
        gestureIndicator.backgroundColor = UIColor.green
    }
    @IBOutlet weak var gestureIndicator: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

